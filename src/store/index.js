import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({

    state: {
        isPreload: false,
        isPreloadDadata: false,
        api: 'http://promindsl.ru/public/api/', //url swagger
        proxyurl: 'http://localhost:3010/', //url local server cors-anywhere
        token: "b8ccb67cf60be58fd7cc52360b5a3b1ea39735c0",
        secret: "887fd8fb14db294a2f7bc9fcc601e62b864cc603",
        content: null,
        dadata: null,
    },

    mutations: {
        setPreload: function (state, payload) {
          state.isPreload = payload;
        },
        setPreloadDadata: function (state, payload) {
          state.isPreloadDadata = payload;
        },
        setRoute: function (state, payload) {
          state.api = payload;
        },
        setContent: function (state, payload) {
          state.content = payload;
        },
        setDadata: function (state, payload) {
          state.dadata = payload;
        },
        clearContent: function (state) {
          state.content = null;
        },
        clearDadata: function (state) {
          state.dadata = null;
        },
    },

    actions: {
        getApi({commit}, payload) {
            commit('clearContent')
            commit('setPreload', true)

            return new Promise((resolve, reject) => {
            axios({url: `${this.state.api}${payload.api}` , method: payload.method})
              .then(response => {
                resolve(response)
                commit('setContent', response.data)
              })
              .catch(err => {
                reject(err)
                console.log('error')
              })
              .finally( () => {
                commit('setPreload', false)
              })
            })
        },

        api({commit}, payload) {
          commit('setPreload', true)

          return new Promise((resolve, reject) => {
            axios({url: `${this.state.api}${payload.api}` , method: payload.method, data: payload.data})
              .then(response => {
                resolve(response)
              })
              .catch(err => {
                reject(err)
              })
              .finally( () => {
                commit('setPreload', false)
              })
            })
        },

        getDaData({commit}, payload) {
            commit('setPreloadDadata', true)

            const url = "https://cleaner.dadata.ru/api/v1/clean";

            const query = {
              structure: [ "NAME", "PHONE", "ADDRESS" ],
              data: [
                [
                  payload.fio,
                  payload.phone,
                  payload.address
                ]
              ]
            };

            const options = {
              method: "POST",
              mode: "cors",
              headers: {
                "Content-Type": "application/json",
                Authorization: "Token " + this.state.token,
                "X-Secret": this.state.secret,
              },
            };
            
            return new Promise((resolve, reject) => {
              axios({
                url: `${this.state.proxyurl}${url}`,
                method: options.method,
                data: JSON.stringify(query),
                headers: options.headers,
                mode: options.mode,
              })
                .then(response => {
                  resolve(response)
                  commit('setDadata', response.data.data[0])
                })
                .catch(err => {
                  reject(err)
                })
                .finally( () => {
                  commit('setPreloadDadata', false)
                })
            })
          },

    },

    
    getters: {
        getApi: (state) => state.api,
        getContent: (state) => state.content,
        getDadata: (state) => state.dadata,
        getToken: (state) => state.token,
        getPreload: (state) => state.isPreload, 
        getPreloadDadata: (state) => state.isPreloadDadata,
    },
 
})
