import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: () => import("@/template/Index"),
    },
    {
      path: '/list-student',
      name: 'ListStudent',
      component: () => import("@/template/ListStudent"),
    },
    {
      path: '/student/:id',
      name: 'DetailedViewCard',
      props: true,
      component: () => import("@/template/DetailedViewCard"),
    },
    {
      path: '/editor/:id',
      name: 'EditorCard',
      props: true,
      component: () => import("@/template/EditorCard"),
    },
  ]
})
