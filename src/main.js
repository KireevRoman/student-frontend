// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store';
import router from './router'
import axios from 'axios';
import MessageBar from "@/components/MessageBar";
import Loader from '@/components/Loader';

import MyDirective from '@/components/mixins/style'
Vue.directive('btn', MyDirective)

Vue.config.productionTip = false
Vue.prototype.$axios = axios

Vue.component('Loader', Loader);

/* кастомный нотификатор*/
Vue.prototype.$messageBar= function (text, _style, _timer) {

  if ((typeof text) === 'object' && text.message)
  text = text.message

  const props = {};
  if (text) props['text'] = text;
  if (_style) props['_style'] = _style;
  if (_timer) props['_timer'] = _timer;

  return new Promise((resolve, reject) => {
    const dialog = new Vue({
      methods: {
        closeHandler(fn, arg) {
          return function() {
            dialog.$destroy();
            dialog.$el.parentNode.removeChild(dialog.$el);
          };
        }
      },

      render(h) {
        return h(MessageBar, {
          props:  props ,
          on: {
            confirm: this.closeHandler(resolve),
            cancel: this.closeHandler(reject, new Error('canceled'))
          }
        });
      }
    }).$mount();
    document.body.appendChild(dialog.$el);
  });
}
/* кастомный нотификатор конец*/

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store: store,
  router,
  components: { App },
  template: '<App/>'
})
