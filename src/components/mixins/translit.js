export const translit = {
    methods: {
        translit_ISO9(str) {

                let str_to_iso9 = [];

                const ru = {
                    'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'ë',
                    'ж': 'ž', 'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm',
                    'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 
                    'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'č', 'ш': 'š', 'щ': 'ŝ', 'ъ': 'ʺ', 
                    'ы': 'y', 'ь': 'ʹ', 'э': 'è', 'ю': 'û', 'я': 'â',
                };
                
                for (let i = 0; i < str.length; ++i ) {
                    str_to_iso9.push( ru[ str[i]]  || ru[ str[i].toLowerCase() ] == undefined && str[i] 
                       || ru[ str[i].toLowerCase() ].toUpperCase()
                   );
                }
                
                return str_to_iso9.join('');
            },
    }

}