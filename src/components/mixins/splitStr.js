export const splitStr = {

	methods: {
		splitStr(str) {
			str = str.replace(/\s+/g, ' ').trim()//очищаем от лишних пробелов
			str = str.split(/\s+/).map((w,i) => i>0 ? w.substring(0,1).toUpperCase() : w.charAt(0).toUpperCase() + w.slice(1) + '_').join('')
			return str;
		},
	},

}


