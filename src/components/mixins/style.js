export default {
    bind (el, bindings) {
        el.style.background = bindings.value.background
        el.style.color = bindings.value.color
    }

}